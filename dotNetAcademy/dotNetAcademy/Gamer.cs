namespace dotNetAcademy
{
    class Gamer
    {
        public string name { get; set; }
        public int level { get; set; }
        /// public int points { get; set; }
        public override string ToString()
        {
            return string.Format("Gracz: {0}  Level: {1}", this.name, this.level);
        }
    }
}