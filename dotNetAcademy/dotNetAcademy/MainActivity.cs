﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;

namespace dotNetAcademy
{
    [Activity(Label = "dotNetAcademy", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private EditText _nameTxt;
        private Button _startButton;
        private Button _statisticButton;
        private Button _closeButton;
        private string name;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);
            _nameTxt = FindViewById<EditText>(Resource.Id.txtName);
            _startButton = FindViewById<Button>(Resource.Id.btnStart);
            _statisticButton = FindViewById<Button>(Resource.Id.btnStatistic);
            _closeButton = FindViewById<Button>(Resource.Id.btnClose);
            _startButton.Click += GetStartButtonClick;
            _statisticButton.Click += GetStatisticButtonClick;
            _closeButton.Click += GetCloseButtonClick;
        }

        private void GetStartButtonClick(object sender, EventArgs eventArgs)
        {
            name = _nameTxt.Text;
            if(name == String.Empty)
            {
                Toast.MakeText(this, "Podaj nazwę gracza!", ToastLength.Long).Show();
            }
            else
            {
                int level = 1;
                var gameActivty = new Intent(this, typeof(GameActivity));
                gameActivty.PutExtra("name", name);
                gameActivty.PutExtra("level", level);
                StartActivity(gameActivty);
            }

        }
        private void GetStatisticButtonClick(object sender, EventArgs eventArgs)
        {
            StartActivity(typeof(StatisticActivity));

        }
        private void GetCloseButtonClick(object sender, EventArgs eventArgs)
        {
            Finish();

        }
    }
}

