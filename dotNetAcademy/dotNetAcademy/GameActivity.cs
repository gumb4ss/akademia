using System;
using SQLite;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Graphics;
using System.Timers;


namespace dotNetAcademy
{
    [Activity(Label = "GameActivity")]
    public class GameActivity : Activity
    {
        private ToggleButton[] btns;
        private TextView _txtName, _txtScore, _txtLevel, _txtTime;
        private Timer timer1, timerButtonClick;
        int totalScores = 0;
        int CompleteLevelScores = 100;
        int level = 0;
        
        private int counter, counterButtonClick;
        string name;
        string dbPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "statistic.db3");
        Random rnd = new Random();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Game);
            btns = new ToggleButton[]
            {
                (ToggleButton)FindViewById(Resource.Id.toggleButton1),
                (ToggleButton)FindViewById(Resource.Id.toggleButton2),
                (ToggleButton)FindViewById(Resource.Id.toggleButton3),
                (ToggleButton)FindViewById(Resource.Id.toggleButton4),
                (ToggleButton)FindViewById(Resource.Id.toggleButton5),
                (ToggleButton)FindViewById(Resource.Id.toggleButton6),
                (ToggleButton)FindViewById(Resource.Id.toggleButton7),
                (ToggleButton)FindViewById(Resource.Id.toggleButton8),
                (ToggleButton)FindViewById(Resource.Id.toggleButton9),
                (ToggleButton)FindViewById(Resource.Id.toggleButton10),
                (ToggleButton)FindViewById(Resource.Id.toggleButton11),
                (ToggleButton)FindViewById(Resource.Id.toggleButton12),
                (ToggleButton)FindViewById(Resource.Id.toggleButton13),
                (ToggleButton)FindViewById(Resource.Id.toggleButton14),
                (ToggleButton)FindViewById(Resource.Id.toggleButton15),
                (ToggleButton)FindViewById(Resource.Id.toggleButton16)

            };
            name = Intent.GetStringExtra("name");
            level = Intent.GetIntExtra("level", 0);
            _txtName = FindViewById<TextView>(Resource.Id.txtName);
            _txtName.Text = $@"Name: {name} ";
            
            _txtLevel = FindViewById<TextView>(Resource.Id.txtLevel);
            _txtLevel.Text = $@"Level: {level} ";
            _txtScore = FindViewById<TextView>(Resource.Id.txtScore);
            CompleteLevelScores = 100 * level;
            _txtScore.Text = $@"Points: {totalScores} / {CompleteLevelScores} ";
            _txtTime = FindViewById<TextView>(Resource.Id.txtTime);
            _txtTime.Text = $@"Time : 1:00";
            timer1 = new Timer();
            timer1.Interval = 1000;
            timer1.Elapsed += OnTimedEvent;
            counter = 60;
            timer1.Enabled = true;
            
           
            reset();
            
        }

        public int randomBtn()
        {
            Random random = new Random();
            int randomNum = random.Next(btns.Length);
            return randomNum;
        }

        public void reset()
        {
            for (int i = 0; i < btns.Length; i++)
            {
                btns[i].SetBackgroundColor(Color.DimGray);
                btns[i].Enabled = false;
                btns[i].Checked = false;
            }
            if (counter <= 0)
            {               
                menuFailLvl();
            }
            if (totalScores > CompleteLevelScores)
            {
                timer1.Stop();
                menuNextLvl();
            }
            
            activeButton(randomBtn());
        }

        private void activeButton(int random)
        {
            btns[random].Enabled = true;
            btns[random].SetBackgroundColor(Color.Yellow);
            timerButtonClick = new Timer();
            timerButtonClick.Interval = 1000;
            timerButtonClick.Elapsed += TimerClick;
            counterButtonClick = 5;
            timerButtonClick.Enabled = true;
            btns[random].Click += randomButtonClick;
        }
        private void randomButtonClick(object sender, EventArgs eventArgs)
        {
            timerButtonClick.Close();
            if (counter != 0)
            {
                totalScores = totalScores + counterButtonClick;
                RunOnUiThread(() => { _txtScore.Text = $@"Points: {totalScores} / {CompleteLevelScores} "; });

                
                int dice = rnd.Next(0, 3);

                string diceMessaage = null;
                switch (dice)
                {
                    case 0:
                        diceMessaage = "Wow!";
                        
                        break;
                    case 1:
                        diceMessaage = "Nice!";
                       
                        break;
                    case 2:
                        diceMessaage = "Perfect!";
                       
                        break;
                    case 3:
                        diceMessaage = "Impossible!";
                        
                        break;      
                    default:
                        break;
                   
                }
                Toast.MakeText(this, diceMessaage, ToastLength.Short).Show();
                
            }
            reset();
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {

            counter--;
            RunOnUiThread(() => { _txtTime.Text = $@"Time : {counter}"; });
            if(counter == 0)
            {
                
                timer1.Stop();

                
            }
        }

        private void TimerClick(object sender, ElapsedEventArgs e)
        {

            counterButtonClick--;
            
            if (counterButtonClick == 0)
            {

                timerButtonClick.Stop();
                reset();

            }
        }

        private void menuNextLvl()
        {
            using (var alertDialog = new AlertDialog.Builder(this))
            {
                alertDialog.SetTitle("wygrana");
                alertDialog.SetMessage("Brawo!uzyska�e� wystarczaj�co du�o punkt�w aby przej�� do kolejnego poziomu!");
                alertDialog.SetPositiveButton("Kolejny Poziom", OkAction);
                alertDialog.SetNegativeButton("Koniec", CancelAction);
                alertDialog.Create().Show();
            }
        }

        private void menuFailLvl()
        {
            using (var alertDialog = new AlertDialog.Builder(this))
            {
                alertDialog.SetTitle("przegrana");
                alertDialog.SetMessage("Uzyskales za ma�o punkt�w aby przej�� do kolejnego poziomu.");
                alertDialog.SetPositiveButton("Powt�rz Poziom", AgainAction);
                alertDialog.SetNegativeButton("Koniec", CancelAction);
                alertDialog.Create().Show();
            }
        }

        private void AgainAction(object sender, DialogClickEventArgs e)
        {
            Finish();
            var gameActivty = new Intent(this, typeof(GameActivity));
            gameActivty.PutExtra("name", name);
            gameActivty.PutExtra("level", level);
            StartActivity(gameActivty);
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            level++;
            Finish();
            var gameActivty = new Intent(this, typeof(GameActivity));
            gameActivty.PutExtra("name", name);
            gameActivty.PutExtra("level", level);
            StartActivity(gameActivty);
        }

        private void CancelAction(object sender, DialogClickEventArgs e)
        {
            Gamer gamer = new Gamer();
            gamer.name = name;
            gamer.level = level;

            var db = new SQLiteConnection(dbPath);
            try
            {
                db.Insert(gamer);
            }
            catch
            {
                db.CreateTable<Gamer>();
                db.Insert(gamer);
            }
            Finish();
            StartActivity(typeof(MainActivity));

            
        }

    }
}