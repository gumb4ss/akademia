using System.Collections.Generic;
using System.Linq;
using SQLite;
using Android.App;
using Android.OS;
using Android.Widget;
using System.IO;

namespace dotNetAcademy
{
    [Activity(Label = "StatisticActivity")]
    public class StatisticActivity : Activity
    {
        private ListView _statisticListView;
        private List<Gamer> gamers;
        string dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "statistic.db3");

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Statistic);
            _statisticListView = FindViewById<ListView>(Resource.Id.StatisticListView);
            getStatisticList();
        }

        private void getStatisticList()
        {
            try
            {        
                var db = new SQLiteConnection(dbPath);
                var jp = db.Table<Gamer>();
                gamers = jp.OfType<Gamer>().ToList();
                ArrayAdapter arrAdapter = new ArrayAdapter(this, Resource.Layout.ListViewDataStatistic, gamers);
                RunOnUiThread(() => _statisticListView.Adapter = arrAdapter);
            }
            catch
            {
               
            }
        }

    }
}